package event.events;

import event.EventDispatcher;
import event.IEvent;
import listeners.ICallListener;

import java.util.List;

public class CallEndedEvent implements IEvent {
    private int callId;

    public CallEndedEvent(int callId) {
        this.callId = callId;
    }

    @Override
    public void run() {
        List<ICallListener> listenerList =
                EventDispatcher.instance.getAllObjectsImplementingInterface(ICallListener.class);
        for (ICallListener listener : listenerList){
            listener.callEnded(callId);
        }
    }

    @Override
    public String toString() {
        return String.valueOf(callId);
    }
}
