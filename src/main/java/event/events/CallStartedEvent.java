package event.events;

import event.EventDispatcher;
import event.IEvent;
import listeners.ICallListener;

import java.util.List;

public class CallStartedEvent implements IEvent {
    private int callId;

    public CallStartedEvent(int callId) {
        this.callId = callId;
    }

    @Override
    public void run() {
        List<ICallListener> listenerList =
                EventDispatcher.instance.getAllObjectsImplementingInterface(ICallListener.class);
        for (ICallListener listener : listenerList){
            listener.callStarted(callId);
        }
    }

    @Override
    public String toString() {
        return String.valueOf(callId);
    }
}
