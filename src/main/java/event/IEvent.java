package event;

public interface IEvent {
    void run();
}
