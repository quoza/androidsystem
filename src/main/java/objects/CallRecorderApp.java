package objects;

import listeners.ICallListener;

public class CallRecorderApp extends Application implements ICallListener {
    @Override
    public void callStarted(int callId) {
        System.out.println("Rozmowa rozpoczela sie w objects.CallRecorderApp.");
    }

    @Override
    public void callEnded(int callId) {
        System.out.println("Rozmowa zakonczyla sie w objects.CallRecorderApp.");
    }
}
