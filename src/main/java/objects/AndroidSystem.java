package objects;

import event.EventDispatcher;
import listeners.ICallListener;

import java.util.ArrayList;
import java.util.List;

public class AndroidSystem implements ICallListener {

    private List<Notification> notifications;
    private List<Integer> currentCalls = new ArrayList<>();

    public AndroidSystem() {
        EventDispatcher.instance.registerObject(this);
    }

    @Override
    public void callStarted(int callId) {
        if (!currentCalls.contains(callId)) {
            currentCalls.add(callId);
            System.out.println("Rozmowa rozpoczela sie w AndroidSystem.");
        } else {
            System.out.println("Rozmowa z tym numerem juz trwa.");
        }
    }

    @Override
    public void callEnded(int callId) {
        if (currentCalls.contains(callId)) {
            currentCalls.remove((Integer) callId);
            System.out.println("Rozmowa zakonczyla sie w AndroidSystem.");
        } else {
            System.out.println("Rozmowa z tym numerem nie istnieje!");
        }
    }
}
