package objects;

import java.time.Duration;
import java.time.LocalDateTime;

public class Call {
    private LocalDateTime beginTime;
    private LocalDateTime endTime;
    private int callerId;
    private int receiverId;
    private Duration duration;
    private int callEnderId;

    public LocalDateTime getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(LocalDateTime beginTime) {
        this.beginTime = beginTime;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    public int getCallerId() {
        return callerId;
    }

    public void setCallerId(int callerId) {
        this.callerId = callerId;
    }

    public int getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(int receiverId) {
        this.receiverId = receiverId;
    }

    public Duration getDuration() {
        return duration;
    }

    public void setDuration(Duration duration) {
        this.duration = duration;
    }

    public int getCallEnderId() {
        return callEnderId;
    }

    public void setCallEnderId(int callEnderId) {
        this.callEnderId = callEnderId;
    }

    private Call(LocalDateTime beginTime, LocalDateTime endTime, int callerId, int receiverId, int callEnderId) {
        this.beginTime = beginTime;
        this.endTime = endTime;
        this.callerId = callerId;
        this.receiverId = receiverId;
        this.callEnderId = callEnderId;
        this.duration = Duration.between(beginTime,endTime);


    }

    public static class CallBuilder {
        private LocalDateTime beginTime;
        private LocalDateTime endTime;
        private int callerId;
        private int receiverId;
        private int callEnderId;

        public CallBuilder setBeginTime(LocalDateTime beginTime) {
            this.beginTime = beginTime;
            return this;
        }

        public CallBuilder setEndTime(LocalDateTime endTime) {
            this.endTime = endTime;
            return this;
        }

        public CallBuilder setCallerId(int callerId) {
            this.callerId = callerId;
            return this;
        }

        public CallBuilder setReceiverId(int receiverId) {
            this.receiverId = receiverId;
            return this;
        }

        public CallBuilder setCallEnderId(int callEnderId) {
            this.callEnderId = callEnderId;
            return this;
        }

        public Call createCall() {
            return new Call(beginTime, endTime, callerId, receiverId, callEnderId);
        }
    }
}