package objects;

import event.EventDispatcher;

public abstract class Application {

    public Application() {
        EventDispatcher.instance.registerObject(this);
    }
}
