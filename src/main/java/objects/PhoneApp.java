package objects;

import listeners.ICallListener;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class PhoneApp extends Application implements ICallListener {
    private List<Call> callList = new ArrayList<>();
    private Call.CallBuilder callBuilder = new Call.CallBuilder();

    @Override
    public void callStarted(int callId) {
        callBuilder.setCallerId(callId).setBeginTime(LocalDateTime.now()).createCall();
        System.out.println("Rozmowa rozpoczela sie w objects.PhoneApp.");
    }

    @Override
    public void callEnded(int callId) {
        System.out.println("Rozmowa zakonczyla sie w objects.PhoneApp.");
    }
}
