import event.EventDispatcher;
import event.events.CallEndedEvent;
import event.events.CallStartedEvent;
import objects.AndroidSystem;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        new AndroidSystem();

        Scanner sc = new Scanner(System.in);
        while(sc.hasNextLine()){
            String line = sc.nextLine();
            String command = line.split(" ")[0];
            int callId = Integer.parseInt(line.split(" ")[1]);
            if(command.equalsIgnoreCase("start")){
                EventDispatcher.instance.dispatch(new CallStartedEvent(callId));
            } else if (command.equalsIgnoreCase("stop")){
                EventDispatcher.instance.dispatch(new CallEndedEvent(callId));
            }
        }
    }
}
